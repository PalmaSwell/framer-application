# Mauricio Palma Assignment

> Imaginary removals company prototype with Framer X

## Description

With this approach, I tried the combined the power of code components with design components. Taking advantage of production-ready React components and interactions together with Framers built-in powerful screen transitions to create the prototype. The code components are considered to be production-ready and can be used by any developer who will implement the product.

There are a few things I would have liked to try, like adding Property Controls to the React components to edit them on the panel. Base animations and transition using Framer Motion. Last but not least, I would have liked to explore using the built-in components with production-ready components without compromising the separation of concerns.

## Requirements

🎨 Framer X
🚀 Node.js >= 10
🌲 Git
🐈 yarn >= 1.12

## Getting started

```bash
  git clone mauricio-palma-assignment.framerfx

  cd mauricio-palma-assignment.framerfx
  # install packages
  yarn
```
