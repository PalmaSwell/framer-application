import * as React from "react";
import { TruckIcon, TruckSize } from "./Truck_Icon";
import { BtnGroup } from "./button-group";
import { Btn } from "./button";

export function Stage(): JSX.Element {
  const [size, setSize] = React.useState(TruckSize.S);
  return (
    <>
      <TruckIcon size={size} />
      <BtnGroup>
        <Btn
          onClick={() => setSize(TruckSize.S)}
          isActive={size === TruckSize.S}
        >
          Pickup
        </Btn>
        <Btn
          onClick={() => setSize(TruckSize.M)}
          isActive={size === TruckSize.M}
        >
          Van
        </Btn>
        <Btn
          onClick={() => setSize(TruckSize.L)}
          isActive={size === TruckSize.L}
        >
          Cube Van
        </Btn>
      </BtnGroup>
    </>
  );
}
