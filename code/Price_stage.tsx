import * as React from "react";
import { Price } from "./price";
import { InputSlider, SliderValue } from "./input-slider";

const total = {
  min: 65,
  max: 155,
};

const RANGE = total.max - total.min;
const INIT_VALUE = Math.floor(50 * (RANGE / 100) + total.min);

export const PriceStage: React.FC = (): JSX.Element => {
  const [percent, setPercent] = React.useState({ x: 50 });
  const [value, setValue] = React.useState(INIT_VALUE);

  const handleChange = (coor: SliderValue): void => {
    setPercent({ x: coor.x });
    setValue(Math.floor(coor.x * (RANGE / 100) + total.min));
  };

  return (
    <>
      <Price label="Estimated total price" value={value} />
      <InputSlider
        min={0}
        max={100}
        step={1}
        value={percent.x}
        onChange={coor => handleChange(coor)}
      />
    </>
  );
};

PriceStage.displayName = "PriceStage";
