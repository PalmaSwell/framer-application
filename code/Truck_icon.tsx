/**@jsx jsx */
import { jsx, SxStyleProp } from "theme-ui";
import { Truck } from "react-feather";
import { qt } from "./theme";

export interface TruckIconProps {
  size: TruckSize;
}

export enum TruckSize {
  S = "S",
  M = "M",
  L = "L",
}

const BASE_SIZE = (qt("space")(10) as number) * 1.6;

const stylesIcon: SxStyleProp = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transformOrigin: "0 0",
  transition: "transform 0.333s linear",
};

const stylesWrapper: SxStyleProp = {
  position: "relative",
  height: `${BASE_SIZE}px`,
};

const createStyles = (size: TruckSize) => ({
  ...stylesIcon,
  transform: `scale(${handleSize(size)}) translate(-50%,-50%)`,
});

function handleSize(size: TruckSize): number {
  switch (size) {
    case TruckSize.S:
      return 0.5;
    case TruckSize.M:
      return 0.75;
    case TruckSize.L:
      return 1;
  }
}

export const TruckIcon: React.FC<TruckIconProps> = (props): JSX.Element => (
  <div sx={stylesWrapper}>
    <Truck
      sx={createStyles(props.size)}
      size={BASE_SIZE}
      color={(qt("lemonChiffon") as unknown) as string}
    />
  </div>
);

TruckIcon.displayName = "TruckIcon";
