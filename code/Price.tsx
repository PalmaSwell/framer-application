/**@jsx jsx */
import { jsx, SxStyleProp } from "theme-ui";
import { qt } from "./theme";

export interface PriceProps {
  label: string;
  value: number;
}

const stylesWrapper: SxStyleProp = {
  display: "block",
  margin: `${qt("space")(9)}px ${qt("space")(8)}px ${qt("space")(11)}px`,
  textAlign: "center",
};

const stylesCaption: SxStyleProp = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  width: "100%",
};

const stylesValue: SxStyleProp = {
  display: "inline-block",
  width: '150px',
  textAlign: "right",
  fontSize: `${qt("fontSizes")(8)}px`,
};

const stylesLabel: SxStyleProp = {
  display: "inline-block",
  maxWidth: "80px",
  color: qt("purpleNavy"),
  fontWeight: "bold",
  fontSize: `${qt("fontSizes")(2)}px`,
};

export const Price: React.FC<PriceProps> = (props): JSX.Element => (
  <figure sx={stylesWrapper}>
    <figcaption sx={stylesCaption}>
      <small sx={stylesLabel}>{props.label}</small>
      <strong sx={stylesValue}>${props.value}</strong>
    </figcaption>
  </figure>
);

Price.displayName = "Price";
