/**@jsx jsx */
import { jsx, SxStyleProp } from "theme-ui";
import { qt } from "./theme";

export interface BtnProps {
  isActive: boolean;
  onClick: React.MouseEventHandler<HTMLButtonElement>;
}

const stylesBtn: SxStyleProp = {
  height: `${qt("space")(10)}px`,
  border: "none",
  backgroundColor: qt("purpleNavy"),
  color: qt("lavendelGray"),
  textAlign: "center",
  fontSize: `${qt("space")(4)}px`,
  ":first-of-type": {
    borderRadius: "6px 0 0 6px",
  },
  ":last-of-type": {
    borderRadius: "0 6px 6px 0",
  },
};

const createStylesBtn = (isActive: boolean): SxStyleProp => ({
  ...stylesBtn,
  backgroundColor: isActive ? qt("lemonChiffon") : qt("purpleNavy"),
});

export const Btn: React.FC<BtnProps> = (props): JSX.Element => (
  <button sx={createStylesBtn(props.isActive)} onClick={props.onClick}>
    {props.children}
  </button>
);

Btn.displayName = "Btn";
