import * as ThemeQuery from "theme-query";

export const theme = {
  fonts: {
    body: "system-ui, sans-serif",
    heading: '"Avenir Next", sans-serif',
  },
  colors: {
    text: "#000",
    background: "#fff",
    primary: "#33e",
    lavendelGray: "#C5C4CE",
    lemonChiffon: "#FFFBC7",
    cyberGrape: "#3D417C",
    purpleNavy: "#4C4F8F",
  },
  space: [0, 3, 6, 12, 18, 24, 30, 36, 48, 72, 96, 140],
  fontSizes: [12, 14, 16, 18, 20, 24, 32, 44, 58],
};

export const qt = ThemeQuery.create({ theme, styles: "object" });
