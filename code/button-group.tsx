/**@jsx jsx */
import { jsx, SxStyleProp } from "theme-ui";
import { qt } from './theme';

const stylesBtnGroup: SxStyleProp = {
  display: "grid",
  gridTemplateColumns: "1fr 1fr 1fr",
  margin: `${qt('space')(10)}px auto 0`,
};

export const BtnGroup: React.FC = (props): JSX.Element => (
  <div sx={stylesBtnGroup}>{props.children}</div>
);

BtnGroup.displayName = "BtnGroup";
