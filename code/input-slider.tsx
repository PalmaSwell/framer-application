import * as React from "react";
import { SxStyleProp } from "theme-ui";
import Slider from "react-input-slider";
import { qt } from "./theme";

export interface InputSliderProps {
  step: number;
  min: number;
  max: number;
  value: number;
  onChange(v: SliderValue): void;
}

export interface SliderValue {
  x: number;
  y: number;
}

const stylesThumb: SxStyleProp = {
  top: qt("space")(0),
  width: qt("space")(8),
  height: qt("space")(8),
  transform: "translate(0, -50%)",
};

const stylesTrackActive: SxStyleProp = {
  backgroundColor: qt("purpleNavy"),
};

const stylesTrack: SxStyleProp = {
  width: "100%",
  height: "2px",
};

const stylesSlider: SxStyleProp = {
  thumb: { ...stylesThumb },
  active: { ...stylesTrackActive },
  track: { ...stylesTrack },
};

export const InputSlider: React.FC<InputSliderProps> = (props): JSX.Element => (
  <Slider
    styles={stylesSlider}
    axis="x"
    xstep={props.step}
    xmin={props.min}
    xmax={props.max}
    x={props.value}
    onChange={props.onChange}
  />
);

InputSlider.displayName = "InputSlider";
